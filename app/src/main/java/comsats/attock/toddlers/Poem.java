package comsats.attock.toddlers;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class Poem extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();

        setContentView(R.layout.activity_poem);

        setTitle(extras.getString("title"));



        String path = "android.resource://" + getPackageName() + "/" ;

        if(extras.getString("file").equals("v_1")){
            path += + R.raw.v_1;
        } else if(extras.getString("file").equals("v_2")){
            path += + R.raw.v_2;
        }else if(extras.getString("file").equals("v_3")){
            path += + R.raw.v_3;
        }else if(extras.getString("file").equals("v_4")){
            path += + R.raw.v_4;
        }else if(extras.getString("file").equals("v_5")){
            path += + R.raw.v_5;
        } else if(extras.getString("file").equals("words_a")){
            path += + R.raw.words_a;
        } else if(extras.getString("file").equals("words_u")){
            path += + R.raw.words_u;
        }



        try {

            VideoView view = (VideoView) findViewById(R.id.videoView);

            MediaController mc = new MediaController(this);
            view.setMediaController(mc);
            view.setVideoURI(Uri.parse(path));
            view.start();

        } catch(Exception e){
            finish();
        }


    }






}
