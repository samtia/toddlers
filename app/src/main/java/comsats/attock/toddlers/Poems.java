package comsats.attock.toddlers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class Poems extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    ListView listView;
    String[] values;
    ArrayAdapter<String> adapter;

    public Poems() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Poems.
     */
    // TODO: Rename and change types and number of parameters
    public static Poems newInstance(String param1, String param2) {
        Poems fragment = new Poems();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // Defined Array values to show in ListView
        values = new String[] { "Bismillah",
                "Brush Teeths",
                "Chocolates",
                "Buckle Your Shoes",
                "Turtle"
        };




        adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vv  =  inflater.inflate(R.layout.fragment_poems, container, false);

        listView = (ListView) vv.findViewById(R.id.poemslist);

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                int itemPosition     = position;

                // ListView Clicked item value
                String  itemValue    = (String) listView.getItemAtPosition(position);


                switch (position){

                    case 0:{

                        StartView("Bismillah","v_1");

                        break;
                    }
                    case 1:{

                        StartView(values[position],"v_2");
                        break;
                    }
                    case 2:{
                        StartView(values[position],"v_3");
                        break;
                    }
                    case 3:{
                        StartView(values[position],"v_4");
                        break;
                    }
                    case 4:{

                        StartView(values[position],"v_5");
                        break;
                    }
                }


            }
        });





        return vv;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentMessage("POEMS", uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MenuActivity) getActivity()).setTitle("Poems");
    }


    public void StartView(String title,String file){

        Intent i = new Intent(getActivity(),Poem.class);
        i.putExtra("title",title);
        i.putExtra("file",file);
        startActivity(i);
    }


}
