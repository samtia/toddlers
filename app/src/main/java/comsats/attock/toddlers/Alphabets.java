package comsats.attock.toddlers;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.speech.tts.Voice;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.Locale;

import comsats.attock.toddlers.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link Alphabets#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Alphabets extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    TextToSpeech t1;

    Button a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;

    private OnFragmentInteractionListener mListener;

    public Alphabets() {

        t1 = null;

        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Alphabets.
     */
    // TODO: Rename and change types and number of parameters
    public static Alphabets newInstance(String param1, String param2) {
        Alphabets fragment = new Alphabets();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }




    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vvv =  inflater.inflate(R.layout.fragment_alphabets, container, false);



        a = (Button) vvv.findViewById(R.id.a);
        b = (Button) vvv.findViewById(R.id.b);
        c = (Button) vvv.findViewById(R.id.c);
        d = (Button) vvv.findViewById(R.id.d);
        e = (Button) vvv.findViewById(R.id.e);
        f = (Button) vvv.findViewById(R.id.f);
        g = (Button) vvv.findViewById(R.id.g);
        h = (Button) vvv.findViewById(R.id.h);
        i = (Button) vvv.findViewById(R.id.i);
        j = (Button) vvv.findViewById(R.id.j);
        k = (Button) vvv.findViewById(R.id.k);
        l = (Button) vvv.findViewById(R.id.l);
        m = (Button) vvv.findViewById(R.id.m);
        n = (Button) vvv.findViewById(R.id.n);
        o = (Button) vvv.findViewById(R.id.o);
        p = (Button) vvv.findViewById(R.id.p);
        q = (Button) vvv.findViewById(R.id.q);
        r = (Button) vvv.findViewById(R.id.r);
        s = (Button) vvv.findViewById(R.id.s);
        t = (Button) vvv.findViewById(R.id.t);
        u = (Button) vvv.findViewById(R.id.u);
        v = (Button) vvv.findViewById(R.id.v);
        w = (Button) vvv.findViewById(R.id.w);
        x = (Button) vvv.findViewById(R.id.x);
        y = (Button) vvv.findViewById(R.id.y);
        z = (Button) vvv.findViewById(R.id.z);

        a.setOnClickListener(this);
        b.setOnClickListener(this);
        c.setOnClickListener(this);
        d.setOnClickListener(this);
        e.setOnClickListener(this);
        f.setOnClickListener(this);
        g.setOnClickListener(this);
        h.setOnClickListener(this);
        i.setOnClickListener(this);
        j.setOnClickListener(this);
        k.setOnClickListener(this);
        l.setOnClickListener(this);
        m.setOnClickListener(this);
        n.setOnClickListener(this);
        o.setOnClickListener(this);
        p.setOnClickListener(this);
        q.setOnClickListener(this);
        r.setOnClickListener(this);
        s.setOnClickListener(this);
        t.setOnClickListener(this);
        u.setOnClickListener(this);
        v.setOnClickListener(this);
        w.setOnClickListener(this);
        x.setOnClickListener(this);
        y.setOnClickListener(this);
        z.setOnClickListener(this);


        return vvv;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentMessage("ALPH'",uri);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MenuActivity) getActivity()).setTitle(getString(R.string.frag_alphabets_title));



    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View vv) {


        Button bb = (Button)vv;
        String buttonText =  bb.getText().toString();


        AssetFileDescriptor afd = null;
        try {
            afd = getActivity().getAssets().openFd("alphabets/"+buttonText+".mp3");

            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
            player.prepare();
            player.start();
        } catch (Exception e1) {
            e1.printStackTrace();
        }


    }
}
