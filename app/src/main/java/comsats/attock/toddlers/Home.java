package comsats.attock.toddlers;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class Home extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // R OK

    Button alpha,num,aerobic,english,urdu,poems;


    private OnFragmentInteractionListener mListener;

    public Home() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Home.
     */
    // TODO: Rename and change types and number of parameters
    public static Home newInstance(String param1, String param2) {
        Home fragment = new Home();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vv =  inflater.inflate(R.layout.fragment_home, container, false);

        num = (Button) vv.findViewById(R.id.nums);

        alpha = (Button) vv.findViewById(R.id.alp);

        aerobic = (Button) vv.findViewById(R.id.aerobic);

        urdu = (Button) vv.findViewById(R.id.urdu);

        english = (Button) vv.findViewById(R.id.english);

        poems = (Button) vv.findViewById(R.id.poems);

        num.setOnClickListener(this);
        alpha.setOnClickListener(this);

        aerobic.setOnClickListener(this);
        urdu.setOnClickListener(this);
        english.setOnClickListener(this);
        poems.setOnClickListener(this);




        return vv;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentMessage("HOME",uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MenuActivity) getActivity()).setTitle(getString(R.string.app_name));
    }


    @Override
    public void onClick(View v) {


        Button bb = (Button)v;

        if(bb == num){
            MenuActivity.fragmentManager.beginTransaction()
                    .replace(R.id.main, new Numbers()).addToBackStack("NUM")
                    .commit();
        } else if(bb == alpha){

            MenuActivity.fragmentManager.beginTransaction()
                    .replace(R.id.main, new Alphabets()).addToBackStack("ALP")
                    .commit();
        } else if(bb == aerobic){

            StartView("Aerobic","words_u");
        } else if(bb == urdu){
            StartView("URDU","words_u");
        } else if(bb == english){
            StartView("English","words_a");
        } else if(bb == poems){

            MenuActivity.fragmentManager.beginTransaction()
                    .replace(R.id.main, new Poems()).addToBackStack("POEM")
                    .commit();
        }



    }


    public void StartView(String title,String file){

        Intent i = new Intent(getActivity(),Poem.class);
        i.putExtra("title",title);
        i.putExtra("file",file);
        startActivity(i);
    }

}
