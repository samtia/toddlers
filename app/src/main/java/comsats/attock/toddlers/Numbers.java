package comsats.attock.toddlers;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import comsats.attock.toddlers.R;


public class Numbers extends Fragment implements View.OnClickListener  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;



    Button one,two,three,four,five,six,seven,eight,nine,zero;

    private OnFragmentInteractionListener mListener;

    public Numbers() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Numbers.
     */
    // TODO: Rename and change types and number of parameters
    public static Numbers newInstance(String param1, String param2) {
        Numbers fragment = new Numbers();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vvv =  inflater.inflate(R.layout.fragment_numbers, container, false);

        one = (Button) vvv.findViewById(R.id.n_1);

        two = (Button) vvv.findViewById(R.id.n_2);
        three = (Button) vvv.findViewById(R.id.n_3);
        four = (Button) vvv.findViewById(R.id.n_4);
        five = (Button) vvv.findViewById(R.id.n_5);
        six = (Button) vvv.findViewById(R.id.n_6);
        seven = (Button) vvv.findViewById(R.id.n_7);
        eight = (Button) vvv.findViewById(R.id.n_8);
        nine = (Button) vvv.findViewById(R.id.n_9);
        zero = (Button) vvv.findViewById(R.id.n_0);


        one.setOnClickListener(this);
        two.setOnClickListener(this);
        three.setOnClickListener(this);
        four.setOnClickListener(this);
        five.setOnClickListener(this);
        six.setOnClickListener(this);
        seven.setOnClickListener(this);
        eight.setOnClickListener(this);
        nine.setOnClickListener(this);
        zero.setOnClickListener(this);


        return vvv;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentMessage("Num", uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MenuActivity) getActivity()).setTitle(getString(R.string.frag_numbers_title));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {


        Button bb = (Button)v;
        String buttonText =  bb.getText().toString();


        AssetFileDescriptor afd = null;
        try {
            afd = getActivity().getAssets().openFd("numbers/"+buttonText+".mp3");

            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
            player.prepare();
            player.start();
        } catch (Exception e1) {
            e1.printStackTrace();
        }


    }
}
