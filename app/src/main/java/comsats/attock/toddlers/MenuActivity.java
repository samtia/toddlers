package comsats.attock.toddlers;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.support.v4.app.FragmentManager;

import comsats.attock.toddlers.R;

public class MenuActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnFragmentInteractionListener {


    public static FragmentManager fragmentManager;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        fragmentManager = getSupportFragmentManager();




        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        fragmentManager.beginTransaction()
                .replace(R.id.main, new Home()).addToBackStack("HOME")
                .commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

            int count = getSupportFragmentManager().getBackStackEntryCount();

            if (count <= 1) {
                super.onBackPressed();
                finish();
                //additional code
            } else {
                getSupportFragmentManager().popBackStack();
            }

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_alphabets) {
            // Handle the camera action


            fragmentManager.beginTransaction()
                    .replace(R.id.main, new Alphabets()).addToBackStack("ALP")
                    .commit();

        } else if (id == R.id.nav_numbers) {

            fragmentManager.beginTransaction()
                    .replace(R.id.main, new Numbers()).addToBackStack("NUM")
                    .commit();

        } else if (id == R.id.nav_home) {

            if(fragmentManager.getBackStackEntryCount() > 1) {

                fragmentManager.popBackStack(fragmentManager.getBackStackEntryAt(1).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);

            }
        } else if(id == R.id.nav_poems){

            fragmentManager.beginTransaction()
                    .replace(R.id.main, new Poems()).addToBackStack("POEM")
                    .commit();

        } else if(id == R.id.nav_aerobic){
            StartView("Aerobic","words_u");
        } else if(id == R.id.words_urdu){
            StartView("URDU","words_u");
        } else if(id == R.id.words_english){
            StartView("English","words_a");
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


//    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onFragmentMessage(String TAG, Object data){
        if (TAG.equals("TAGFragment1")){
            //Do something with 'data' that comes from fragment1
        }
        else if (TAG.equals("TAGFragment2")){
            //Do something with 'data' that comes from fragment2
        }
    }



    public void StartView(String title,String file){

        Intent i = new Intent(this,Poem.class);
        i.putExtra("title",title);
        i.putExtra("file",file);
        startActivity(i);
    }


}
